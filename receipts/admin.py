from django.contrib import admin
from receipts.models import ExpenseCategory, Account, Receipt

# Register your models here.
admin.site.register(Account)
admin.site.register(Receipt)
admin.site.register(ExpenseCategory)
